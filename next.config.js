/** @type {import('next').NextConfig} */
const isProd = process.env.NODE_ENV === 'production'
const nextConfig = {
  reactStrictMode: true,
  //assetPrefix: isProd ? 'http://xpeed.studio.infoagebd.com' : '',
  assetPrefix: isProd ? 'http://localhost/frontend-xpeedstudio' : '',
}
module.exports = nextConfig
