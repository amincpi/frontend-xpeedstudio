import { Layout } from 'antd';
const { Header, Footer, Sider, Content } = Layout;
const HeaderTopBar=()=>{
    return(
        <Layout className="layout">
            <Header className='header_topbar'>
                <div className="d-flex logo">
                    <div className='align-self-center'>
                        <h3 className='mn'>XpeedStudio</h3>
                        {/* <h3 className='mn'>Test</h3> */}
                    </div>
                </div>
            </Header>
        </Layout>
    )
}
export default HeaderTopBar;