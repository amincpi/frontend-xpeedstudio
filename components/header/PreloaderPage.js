import { Spin } from 'antd';
const PreloaderPage = () => {
    return (
        <div className='w-100 d-flex align-items-center justify-content-center' style={{height: '100vh'}}>
            <Spin />
        </div>
    )
}
export default PreloaderPage;