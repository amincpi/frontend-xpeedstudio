import React from 'react';
import Head from './modules/Head';
const DefaultLayout = ({ pages }) => (
    <div className="div_root">
        <Head />
        {pages}
    </div>
);

export default DefaultLayout;