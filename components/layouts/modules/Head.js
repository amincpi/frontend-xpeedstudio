import React from 'react';
import Head from 'next/head';
const StyleSheets = () => (
    <Head>
        <title>XpeedStudio Assignment</title>
        <meta httpEquiv="X-UA-C
        ompatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="author" content="XpeedStudio" />
        <meta name="keywords" content="XpeedStudio" />
        <meta name="description" content="XpeedStudio" />
        <link rel="icon" type="image/x-icon" href="/assets/images/task_favicon.ico"></link>
        <link
            href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext"
            rel="stylesheet"
        />
    </Head>
);
export default StyleSheets;