import React, {useEffect, useState} from 'react';
import { useRouter } from 'next/router';
import { getCookie } from 'cookies-next';
import HeaderTopBar from '../header/HeaderTopBar';
import PreloaderPage from '../header/PreloaderPage';

const ContainerHomeDefault = ({ children, title }) => {
    const [ok,setOk] = useState(false);
    useEffect(()=>{

        // let isAuth = false;
        // if(typeof window !== 'undefined'){
        //     isAuth = getCookie('auth');
        // }
        // if(!isAuth){
        //     router.push('/login');
        // }else{
        //     setOk(true);
        // }
        setTimeout(() => {
            setOk(true);
        },500);

    },[]);
    const router = useRouter();
    return ( ok ?
                <>
                    <HeaderTopBar />
                    <div className='container' style={{marginTop:45}}>
                        {children}
                    </div>
                </> : <PreloaderPage/>
            );
};
export default ContainerHomeDefault;
