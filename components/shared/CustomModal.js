import { Modal, Button } from "antd";
import { useState,useEffect } from 'react';
const CustomModal = (props) => {

    const inti_modal = {
        title: "Create",
        visible: false,
    };
    const [modal,setModal]=useState(inti_modal);
    useEffect(() => {
        setModal({
            ...modal,
            visible:props.visible,
            title:props.title,
        });
    },[props]);  
    const handleModalCancel=()=>{
        setModal({
            ...modal,
            visible:false,
        });
    }
    return (
        <Modal
            centered
            className="ant-modal-body"
            maskClosable={false}
            visible={modal.visible}
            title={<h5 className="modal-title fw-bold">{modal.title}</h5>}
            // onOk={handleModalOk}
            onCancel={handleModalCancel}
            // okButtonProps={props.okButtonProps}
            // okText={props.okText}
            footer={props.footer && props.footer ? props.footer : null}>
                {props.children}
        </Modal>
    );
};
export default CustomModal;
