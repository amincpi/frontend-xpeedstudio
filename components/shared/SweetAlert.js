
import { Alert } from 'antd';
import Swal from 'sweetalert2'
export const SwAlertHtmlView=(messages,icon='error')=>{
    const bs_icon={"error" : "danger","success":"success"};
    var ErrorsContent="";
    messages.map(item => {
        ErrorsContent+=`<div class="alert alert-${bs_icon[icon]}" role="alert">
            ${item}
        </div>`;
    });
    Swal.fire({
        icon: icon,
        html:ErrorsContent,
        showCancelButton: false,
        showCloseButton: true,
      })
}