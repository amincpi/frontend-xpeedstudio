import React, { useState,useEffect,useRef } from 'react';
import {
    Input,
    Button,
    Form,
    Select,InputNumber, Alert
  } from 'antd';
import { getCookie,setCookies } from 'cookies-next';
import OrderRepository from '../../../repositories/OrderRepository';
import {SwAlertHtmlView} from '../../shared/SweetAlert';
const { TextArea } = Input;
const { Option } = Select;
const OrderForm=(props)=>{
    const [form] = Form.useForm();
    const [formLayout, setFormLayout] = useState('vertical');
    const [children,setChildren]=useState([]);
    const [loading,setLoading]=useState(false);
    const [errors,setErrors]=useState(null);
    const ref_btn = useRef(null);
    useEffect(() => {
        form.setFieldsValue({
            country_code: "880",
        });
    },[]);
    useEffect(() => {
        if(props.isBtnSave)
            ref_btn.current.click();
    },[props.isBtnSave]); 
  
    const onFinish = async(values) => {
       //console.log("values",values); 
       props.handleBtnSave(true,true);
       const phone=values.country_code+""+values.phone;
       if(phone.length < 13)
            SwAlertHtmlView(["Valid phone is required."],"error");
       else{
            setLoading(true);
            setErrors(null);
            const api=await OrderRepository.store(values);
            if(api && api.error == undefined){
                onReset();
                SwAlertHtmlView(["Success"],"success");
                setCookies("auth",true,{maxAge:43200});
                await props.handleBtnSave(true,false);
                //await props.handleList(api.result);
            }
            else if(api && Array.isArray(api.error.response) && api.error.response != undefined){
                    SwAlertHtmlView(api.error.response.data);
                    // setErrors(errorsContent);
                    // setTimeout(() => {
                    //     setErrors(null);
                    // },5000);
            }
            else
                SwAlertHtmlView(["Something went wrong!!!"]);
        }
        props.handleBtnSave(true,false);
        //console.log("api",api.error.response.data);
        setLoading(false);
    };
    const onReset = () => {
        form.resetFields();
        form.setFieldsValue({
            country_code: "880",
        });
    };
    const onFinishFailed = (errorInfo) => {
        props.handleBtnSave(true);
        //console.log('Failed:', errorInfo);
    };
    function handleChange(value) {
        console.log(`selected ${value}`);
    }
    const prefixSelector = (
        <Form.Item name="country_code" size="small" noStyle>
          <Select
                defaultValue="880"
                style={{
                width: 70,
                }}>
                    <Option value="880">+880</Option>
            </Select>
        </Form.Item>
      );
    return(
        <div>
            <div>
                {errors}
            </div>
            <div style={{width:'100%'}}>
                <div>
                    <Form
                        labelCol={{
                            span: 6,
                        }}
                        wrapperCol={{
                            span: 16,
                        }}
                        form={form}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off">
                        <Form.Item 
                            label="Amount"
                            name="amount" 
                            className="form_input_gap" 
                            rules={[{ required: true,message: 'Please Enter Amount!' }]}>
                                <InputNumber style={{width:'100%'}} size="small" placeholder="Enter Amount" />
                        </Form.Item>
                        <Form.Item 
                            label="Buyer"
                            name="buyer" 
                            className="form_input_gap" 
                            rules={[{ required: true,message: 'Please Enter Buyer!' }]}>
                                <Input showCount maxLength={20} size="small" placeholder="Enter Buyer" />
                        </Form.Item>
                        <Form.Item 
                            label="Receipt Id"
                            name="receipt_id" 
                            className="form_input_gap" 
                            rules={[{ required: true,message: 'Please Enter Receipt Id!' }]}>
                                <Input size="small" placeholder="Enter Receipt Id" />
                        </Form.Item>
                        <Form.Item 
                            label="Items" 
                            name="items"
                            className="form_input_gap" 
                            rules={[{ required: true,message: 'Please Enter Items!' }]}>
                                <Select 
                                    mode="tags" 
                                    style={{ width: '100%' }} 
                                    placeholder="Multiple Item By Press Enter" 
                                    onChange={handleChange}>
                                    {children}
                                </Select>
                        </Form.Item>
                        <Form.Item 
                            label="Buyer Email"
                            name="buyer_email"
                            className="form_input_gap" 
                            rules={[{
                                    type: 'email',
                                    message: 'The input is not valid E-mail!',
                                    },{ required: true }]}>
                                <Input type="email" size="small" placeholder="Enter Buyer Email" />
                        </Form.Item>
                        <Form.Item 
                            label="Note"
                            name="note" 
                            className="form_input_gap" 
                            rules={[{ required: true,message: 'Please Enter Note!' }]}>
                                <TextArea showCount maxLength={30} size="small" rows={2} placeholder="Max 30 words"  />
                        </Form.Item>
                        <Form.Item 
                            label="City"
                            name="city" 
                            className="form_input_gap" 
                            rules={[{ required: true,message: 'Please Enter City!' }]}>
                                <Input size="small" placeholder="Enter City" />
                        </Form.Item>
                        <Form.Item 
                            label="Phone"
                            name="phone"
                            rules={[{ required: true,message: 'Please Enter phone!' }]}
                            className="form_input_gap">
                                <Input
                                    // minLength={10}
                                    size="small"
                                    placeholder="Enter Phone No"
                                    addonBefore={prefixSelector}
                                    style={{
                                        width: '100%',
                                    }}
                                />
                        </Form.Item>
                        <Form.Item 
                            label="Entry By"
                            name="entry_by" 
                            className="form_input_gap" 
                            rules={[{ required: true,message: 'Please Enter Entry By!' }]}>
                                <InputNumber style={{width:'100%'}} size="small" placeholder="Enter Entry By" />
                        </Form.Item>
                        <Form.Item 
                                style={{display:'none'}}
                                wrapperCol={{
                                offset: 8,
                                span: 8,
                            }}>
                            <Button ref={ref_btn} type="hidden" loading={loading} htmlType="submit">
                                Submit
                            </Button>
                            {/* <Button htmlType="button" onClick={onReset}>
                                Reset
                            </Button> */}
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    )
}
export default OrderForm;