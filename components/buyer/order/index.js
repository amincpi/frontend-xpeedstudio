import { useState,useEffect } from 'react';
import { Button,Form,Table } from 'antd';
import FilterTransactions from './filter';
import { PlusCircleFilled, PlusCircleOutlined, SearchOutlined } from '@ant-design/icons';
import CustomModal from '../../shared/CustomModal';
import OrderForm from './create';
import OrderRepository from '../../../repositories/OrderRepository';
import { getCookieInfo,getCookieErrorMsg } from "../../../repositories/Repository";
import { getCookie } from 'cookies-next';
const OrderList=()=>{
    const [data,setData]=useState(null);
    const [loading,setLoading]=useState(false);
    const [loadingSearch,setLoadingSearch]=useState(false);
    const [form] = Form.useForm();
    const [auth,setAuth]=useState(true);
    const inti_modal = {
        title: "New Order",
        visible: false,
        isBtnSave:false,
      };
    useEffect(() => {
        filter({});
        if(typeof window !== 'undefined'){
            const auth_check = getCookieInfo();
            console.log("log",auth_check);
             if(!auth_check)
                 setAuth(false);
        }
    },[]);  
    const [modal_info, setModal_info] = useState(inti_modal);
    const columns = [
        {
          title: 'SL',
          render: (row, record,index) => (<span>{index + 1}</span>)
        },
        {
          title: 'Amount',
          dataIndex: 'amount',
          key: 'amount',
        },
        {
          title: 'Buyer',
          dataIndex: 'buyer',
          key: 'buyer',
        },
        {
          title: 'Receipt Id',
          dataIndex: 'receipt_id',
          key: 'receipt_id',
        },
        {
          title: 'Items',
          dataIndex: 'items',
          key: 'items',
        },
        {
          title: 'Email',
          dataIndex: 'buyer_email',
          key: 'buyer_email',
        },
        {
          title: 'City',
          dataIndex: 'city',
          key: 'receipt_id',
        },
        {
          title: 'Phone',
          dataIndex: 'phone',
          key: 'phone',
        },
        {
          title: 'Entry By',
          dataIndex: 'entry_by',
          key: 'entry_by',
        },
        {
          title: 'Date',
          dataIndex: 'entry_at',
          key: 'entry_at',
        },
        {
          title: 'Note',
          dataIndex: 'note',
          key: 'note',
        },
    ];
    const filter = async(values) => {
        setModal_info({
            ...modal_info,
            visible:false,
        });
        setLoadingSearch(true);
        const api=await OrderRepository.get(values);
        if(api && api.error == undefined)
            setData(api.result);
        setLoadingSearch(false);
    }

    const btnFooter=(<Button
                        size="small"
                        type="primary"
                        loading={loading}
                        onClick={()=>handleBtnSave(false)}
                        icon={<PlusCircleOutlined />} 
                        htmlType="button">
                            Save
                    </Button>)
    const handleModal=()=>{
        setModal_info({
            ...modal_info,
            visible:true,
            isBtnSave:false,
        });
    }
    const handleBtnSave=(mode,isFormSubmit=false)=>{
        console.log("test");
        setModal_info({
            ...modal_info,
            isBtnSave:!mode,
        });
        console.log("loading",isFormSubmit);
        setLoading(isFormSubmit);
    }
    const handleList=(data)=>{
        setModal_info({
            ...modal_info,
            visible:false,
        });
        setData(data);
        setAuth(true);
    }
    ///console.log(data);
    return(
        <>
            <div>
                <div className='row' style={{background:'#fff'}}>
                    <div className='col-10'>
                        <div className="def_div_height d-flex align-items-center">
                            <h2>Transactions</h2>
                        </div>
                    </div>
                    {
                        
                            <div className='col-2'>
                                <div className="def_div_height d-flex justify-content-end align-items-center">
                                    {
                                        !auth ? (
                                            <Button onClick={handleModal} size="small" icon={<PlusCircleFilled />} type="danger">Add</Button>
                                        ) : (<p style={{color:"red"}}>{getCookieErrorMsg()}</p>)
                                    }
                                </div>
                            </div>
                        
                    }
                </div>
                <div className='row' style={{background:'#fff'}}>
                    <div className='col-12'>
                        <div className="def_div_height align-items-center justify-content-center">
                            <Form layout='horizontal' form={form} onFinish={filter} autoComplete="off">
                                <div className="d-flex justify-content-center align-items-center">
                                    <FilterTransactions />
                                    <Form.Item style={{marginLeft:5,marginTop:-10}} className="form_input_gap" name="date_to">
                                        <Button
                                            size="small"
                                            type="primary"
                                            loading={loadingSearch}
                                            icon={<SearchOutlined />} 
                                            htmlType="submit">
                                            Search
                                        </Button>
                                    </Form.Item>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
                <div className='row' style={{marginTop:10}}>
                    <Table
                        style={{width:'100%'}} 
                        className="common_table"
                        columns={columns}
                        spinning={loading}
                        loading="true"
                        size="small"
                        bordered="true"
                        pagination={{ pageSize: 100 }}
                        dataSource={data}
                    />
                </div>
            </div>
            <CustomModal
                footer={btnFooter}
                {...modal_info}>
                <OrderForm 
                    handleList={handleList} 
                    handleBtnSave={handleBtnSave} {...modal_info} />
            </CustomModal>
        </>
    )
}
export default OrderList;