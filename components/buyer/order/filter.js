
import { Select, Form,DatePicker,Input } from 'antd';
const FilterTransactions=()=>{
    return( 
        <>
            <Form.Item label="From Date" className="form_input_gap" name="date_from">
                <DatePicker
                    size="small" 
                    renderExtraFooter={() => "extra footer"}
                    style={{
                        height: "auto",
                        width: "100%",
                    }}
                />
            </Form.Item>   
            <Form.Item label="To Date" className="form_input_gap" name="date_to">
                <DatePicker
                    size="small" 
                    renderExtraFooter={() => "extra footer"}
                    style={{
                        height: "auto",
                        width: "100%",
                    }}
                />
            </Form.Item>   
            <Form.Item label="User Id" className="form_input_gap" name="user_id">
                <Input  size="small" placeholder="Enter Buyer" />
            </Form.Item>
        </>   
    )

}
export default FilterTransactions;