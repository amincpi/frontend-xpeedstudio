import axios from 'axios';
// when local
const baseDomain = 'http://localhost:3000';
//const baseDomainApi = 'https://xpeed.admin.infoagebd.com'
const baseDomainApi = 'http://localhost/backend-xpeedstudio'
export const baseUrl = `${baseDomain}`;
export const baseUrlApi = `${baseDomainApi}`;
export const ApiVersion = "v1/public";
import { getCookie } from 'cookies-next';
export const getCookieInfo=()=>{
    let token = null;
    if(typeof window !== 'undefined'){
        token = getCookie('auth');
    }
    //console.log("token",token);
    return token;
}
export const getCookieErrorMsg=()=>{
    return "Submission is blocked for 24hours since submission time.";
}
export const customHeaders = {
    Accept: 'application/json',
    withCredentials: true,
    //"Set-Cookie": getCookieInfo(),
    //"Authorization" : `Bearer ${getLogInfo()}`,
};
export default axios.create({
    baseUrl,
    headers: customHeaders,
});
export const serializeQuery = (query) => {
    return Object.keys(query)
        .map(
            (key) =>
                `${encodeURIComponent(key)}=${encodeURIComponent(query[key])}`
        )
        .join('&');
};
