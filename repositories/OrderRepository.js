import Repository, { baseUrlApi,getCookieInfo,getCookieErrorMsg } from "./Repository";
import moment from "moment";
import { SwAlertHtmlView } from "../components/shared/SweetAlert";
class OrderRepository {
  async store(data) {

    if(getCookieInfo()){
      SwAlertHtmlView([getCookieErrorMsg()],"error");
      return null;
    }

    const api_url = `${baseUrlApi}/order/store`;
    data.phone=data.country_code+""+data.phone;
    const reponse = await Repository.post(api_url,data)
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return {error: error};
      });
    return reponse;
  }
  async get(data) {

    // if(getCookieInfo()){
    //   SwAlertHtmlView([getCookieErrorMsg()],"error");
    //   return null;
    // }

    const api_url = `${baseUrlApi}/order/list`;
    if(data.date_from != '' && data.date_from != null && data.date_from !== undefined)
      data.date_from=moment(new Date(data.date_from)).format('DD-MM-YYYY');
    if(data.date_to != '' && data.date_to != null && data.date_to !== undefined)
      data.date_to=moment(new Date(data.date_to)).format('DD-MM-YYYY');

    const reponse = await Repository.get(api_url,{params:data})
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return {error: error};
      });
    return reponse;
  }
}
export default new OrderRepository();
