import OrderList from "../components/buyer/order/index";
import ContainerHomeDefault from "../components/layouts/ContainerHomeDefault";
export default function Home() {
  return (
    <ContainerHomeDefault>
          <OrderList />
    </ContainerHomeDefault>
  )
}
