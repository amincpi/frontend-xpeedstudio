import '../styles/globals.css'
import "../assets/css/main.css";
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import "../assets/css/header/header_topbar.css";
import "../assets/css/bootstrap.min.css";
import DefaultLayout from '../components/layouts/DefaultLayout';
function MyApp({ Component, pageProps }) {
  const getLayout =Component.getLayout || ((page) => <DefaultLayout pages={page} />);
  return getLayout(<Component {...pageProps} />);
}
export default MyApp
